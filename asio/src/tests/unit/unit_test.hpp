//
// unit_test.hpp
// ~~~~~~~~~~~~~
//
// Copyright (c) 2003-2016 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef UNIT_TEST_HPP
#define UNIT_TEST_HPP

#include "asio/detail/config.hpp"
#include <iostream>
#include <utility>
#include "asio/detail/atomic_count.hpp"

#if defined(__sun)
# include <stdlib.h> // Needed for lrand48.
#endif // defined(__sun)

#if defined(__BORLANDC__)

// Prevent use of intrinsic for strcmp.
# include <cstring>
# undef strcmp
 
// Suppress error about condition always being true.
# pragma option -w-ccc

#endif // defined(__BORLANDC__)

#if defined(ASIO_MSVC)
# pragma warning (disable:4127)
# pragma warning (push)
# pragma warning (disable:4244)
# pragma warning (disable:4702)
#endif // defined(ASIO_MSVC)

#if !defined(ASIO_TEST_IOSTREAM)
# define ASIO_TEST_IOSTREAM std::cerr
#endif // !defined(ASIO_TEST_IOSTREAM)

namespace asio {
namespace detail {

inline const char*& test_name()
{
  static const char* name = 0;
  return name;
}

inline atomic_count& test_errors()
{
  static atomic_count errors(0);
  return errors;
}

inline void begin_test_suite(const char* name)
{
  asio::detail::test_name();
  asio::detail::test_errors() = 0;
  ASIO_TEST_IOSTREAM << name << " test suite begins" << std::endl;
}

inline long end_test_suite(const char* name)
{
  ASIO_TEST_IOSTREAM << name << " test suite ends" << std::endl;
  ASIO_TEST_IOSTREAM << "\n*** ";
  long errors = asio::detail::test_errors();
  if (errors == 0)
    ASIO_TEST_IOSTREAM << "No errors detected.";
  else if (errors == 1)
    ASIO_TEST_IOSTREAM << "1 error detected.";
  else
    ASIO_TEST_IOSTREAM << errors << " errors detected." << std::endl;
  ASIO_TEST_IOSTREAM << std::endl;
  return errors;
}

template <void (*Test)()>
inline void run_test(const char* name)
{
  test_name() = name;
  long errors_before = asio::detail::test_errors();
  Test();
  if (test_errors() == errors_before)
    ASIO_TEST_IOSTREAM << name << " passed" << std::endl;
  else
    ASIO_TEST_IOSTREAM << name << " failed" << std::endl;
}

template <void (*)()>
inline void compile_test(const char* name)
{
  ASIO_TEST_IOSTREAM << name << " passed" << std::endl;
}

#if defined(ASIO_NO_EXCEPTIONS)

template <typename T>
void throw_exception(const T& t)
{
  ASIO_TEST_IOSTREAM << "Exception: " << t.what() << std::endl;
  std::abort();
}

#endif // defined(ASIO_NO_EXCEPTIONS)

} // namespace detail
} // namespace asio

#define ASIO_CHECK(expr) \
  do { if (!(expr)) { \
    ASIO_TEST_IOSTREAM << __FILE__ << "(" << __LINE__ << "): " \
      << asio::detail::test_name() << ": " \
      << "check '" << #expr << "' failed" << std::endl; \
    ++asio::detail::test_errors(); \
  } } while (0)

#define ASIO_CHECK_MESSAGE(expr, msg) \
  do { if (!(expr)) { \
    ASIO_TEST_IOSTREAM << __FILE__ << "(" << __LINE__ << "): " \
      << asio::detail::test_name() << ": " \
      << msg << std::endl; \
    ++asio::detail::test_errors(); \
  } } while (0)

#define ASIO_WARN_MESSAGE(expr, msg) \
  do { if (!(expr)) { \
    ASIO_TEST_IOSTREAM << __FILE__ << "(" << __LINE__ << "): " \
      << asio::detail::test_name() << ": " \
      << msg << std::endl; \
  } } while (0)

#define ASIO_ERROR(msg) \
  do { \
    ASIO_TEST_IOSTREAM << __FILE__ << "(" << __LINE__ << "): " \
      << asio::detail::test_name() << ": " \
      << msg << std::endl; \
    ++asio::detail::test_errors(); \
  } while (0)

#define ASIO_TEST_SUITE(name, tests) \
  std::pair<const char*, long> name ## _test_suite()  \
  { \
    asio::detail::begin_test_suite(#name); \
    tests \
    return std::make_pair(#name, asio::detail::end_test_suite(#name)); \
  }

#define ASIO_TEST_CASE(test) \
  asio::detail::run_test<&test>(#test);

#define ASIO_COMPILE_TEST_CASE(test) \
  asio::detail::compile_test<&test>(#test);

inline void null_test()
{
}

std::pair<const char*, long> basic_datagram_socket_test_suite();
std::pair<const char*, long> basic_deadline_timer_test_suite();
std::pair<const char*, long> basic_raw_socket_test_suite();
std::pair<const char*, long> basic_seq_packet_socket_test_suite();
std::pair<const char*, long> basic_serial_port_test_suite();
std::pair<const char*, long> basic_signal_set_test_suite();
std::pair<const char*, long> basic_socket_acceptor_test_suite();
std::pair<const char*, long> basic_streambuf_test_suite();
std::pair<const char*, long> basic_stream_socket_test_suite();
std::pair<const char*, long> basic_waitable_timer_test_suite();
std::pair<const char*, long> buffer_test_suite();
std::pair<const char*, long> buffered_read_stream_test_suite();
std::pair<const char*, long> buffered_stream_test_suite();
std::pair<const char*, long> buffered_write_stream_test_suite();
std::pair<const char*, long> buffers_iterator_test_suite();
std::pair<const char*, long> completion_condition_test_suite();
std::pair<const char*, long> connect_test_suite();
std::pair<const char*, long> coroutine_test_suite();
std::pair<const char*, long> datagram_socket_service_test_suite();
std::pair<const char*, long> deadline_timer_test_suite();
std::pair<const char*, long> deadline_timer_service_test_suite();
std::pair<const char*, long> error_test_suite();
std::pair<const char*, long> generic_datagram_protocol_test_suite();
std::pair<const char*, long> generic_stream_protocol_test_suite();
std::pair<const char*, long> generic_basic_endpoint_test_suite();
std::pair<const char*, long> generic_raw_protocol_test_suite();
std::pair<const char*, long> generic_seq_packet_protocol_test_suite();
std::pair<const char*, long> high_resolution_timer_test_suite();
std::pair<const char*, long> io_context_test_suite();
std::pair<const char*, long> ip_address_v4_test_suite();
std::pair<const char*, long> ip_basic_resolver_iterator_test_suite();
std::pair<const char*, long> ip_address_v6_iterator_test_suite();
std::pair<const char*, long> ip_host_name_test_suite();
std::pair<const char*, long> ip_network_v4_test_suite();
std::pair<const char*, long> ip_network_v6_test_suite();
std::pair<const char*, long> ip_address_v4_iterator_test_suite();
std::pair<const char*, long> ip_basic_resolver_entry_test_suite();
std::pair<const char*, long> ip_resolver_service_test_suite();
std::pair<const char*, long> ip_unicast_test_suite();
std::pair<const char*, long> ip_basic_endpoint_test_suite();
std::pair<const char*, long> ip_multicast_test_suite();
std::pair<const char*, long> ip_address_v6_range_test_suite();
std::pair<const char*, long> ip_basic_resolver_test_suite();
std::pair<const char*, long> ip_icmp_test_suite();
std::pair<const char*, long> ip_udp_test_suite();
std::pair<const char*, long> ip_address_test_suite();
std::pair<const char*, long> ip_tcp_test_suite();
std::pair<const char*, long> ip_basic_resolver_query_test_suite();
std::pair<const char*, long> ip_address_v6_test_suite();
std::pair<const char*, long> ip_v6_only_test_suite();
std::pair<const char*, long> ip_resolver_query_base_test_suite();
std::pair<const char*, long> ip_address_v4_range_test_suite();
std::pair<const char*, long> is_read_buffered_test_suite();
std::pair<const char*, long> is_write_buffered_test_suite();
std::pair<const char*, long> local_connect_pair_test_suite();
std::pair<const char*, long> local_datagram_protocol_test_suite();
std::pair<const char*, long> local_stream_protocol_test_suite();
std::pair<const char*, long> local_basic_endpoint_test_suite();
std::pair<const char*, long> placeholders_test_suite();
std::pair<const char*, long> posix_basic_descriptor_test_suite();
std::pair<const char*, long> posix_basic_stream_descriptor_test_suite();
std::pair<const char*, long> posix_stream_descriptor_test_suite();
std::pair<const char*, long> posix_stream_descriptor_service_test_suite();
std::pair<const char*, long> posix_descriptor_base_test_suite();
std::pair<const char*, long> raw_socket_service_test_suite();
std::pair<const char*, long> read_at_test_suite();
std::pair<const char*, long> read_test_suite();
std::pair<const char*, long> read_until_test_suite();
std::pair<const char*, long> seq_packet_socket_service_test_suite();
std::pair<const char*, long> serial_port_base_test_suite();
std::pair<const char*, long> serial_port_test_suite();
std::pair<const char*, long> serial_port_service_test_suite();
std::pair<const char*, long> signal_set_test_suite();
std::pair<const char*, long> signal_set_service_test_suite();
std::pair<const char*, long> socket_acceptor_service_test_suite();
std::pair<const char*, long> socket_base_test_suite();
std::pair<const char*, long> ssl_context_test_suite();
std::pair<const char*, long> ssl_rfc2818_verification_test_suite();
std::pair<const char*, long> ssl_stream_base_test_suite();
std::pair<const char*, long> ssl_stream_test_suite();
std::pair<const char*, long> ssl_context_base_test_suite();
std::pair<const char*, long> steady_timer_test_suite();
std::pair<const char*, long> strand_test_suite();
std::pair<const char*, long> streambuf_test_suite();
std::pair<const char*, long> stream_socket_service_test_suite();
std::pair<const char*, long> system_timer_test_suite();
std::pair<const char*, long> thread_test_suite();
std::pair<const char*, long> time_traits_test_suite();
std::pair<const char*, long> waitable_timer_service_test_suite();
std::pair<const char*, long> wait_traits_test_suite();
std::pair<const char*, long> windows_basic_object_handle_test_suite();
std::pair<const char*, long> windows_object_handle_service_test_suite();
std::pair<const char*, long> windows_basic_stream_handle_test_suite();
std::pair<const char*, long> windows_random_access_handle_service_test_suite();
std::pair<const char*, long> windows_random_access_handle_test_suite();
std::pair<const char*, long> windows_basic_random_access_handle_test_suite();
std::pair<const char*, long> windows_stream_handle_test_suite();
std::pair<const char*, long> windows_basic_handle_test_suite();
std::pair<const char*, long> windows_stream_handle_service_test_suite();
std::pair<const char*, long> windows_overlapped_ptr_test_suite();
std::pair<const char*, long> windows_object_handle_test_suite();
std::pair<const char*, long> write_at_test_suite();
std::pair<const char*, long> write_test_suite();

#if defined(__GNUC__) && defined(_AIX)

// AIX needs this symbol defined in asio, even if it doesn't do anything.
int test_main(int, char**)
{
}

#endif // defined(__GNUC__) && defined(_AIX)

#if defined(ASIO_MSVC)
# pragma warning (pop)
#endif // defined(ASIO_MSVC)

#endif // UNIT_TEST_HPP

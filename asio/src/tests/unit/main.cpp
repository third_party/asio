#include "unit_test.hpp"

#include <utility>
#include <vector>

namespace {

using TestSuite = std::pair<const char*, long> (*)();

TestSuite test_suites[] = {
    // basic_datagram_socket_test_suite,
    basic_deadline_timer_test_suite,
    // basic_raw_socket_test_suite,
    // basic_seq_packet_socket_test_suite,
    // basic_serial_port_test_suite,
    // basic_signal_set_test_suite,
    // basic_socket_acceptor_test_suite,
    basic_streambuf_test_suite,
    // basic_stream_socket_test_suite,
    // basic_waitable_timer_test_suite,
    // buffer_test_suite,
    // buffered_read_stream_test_suite,
    // buffered_stream_test_suite,
    // buffered_write_stream_test_suite,
    buffers_iterator_test_suite,
    completion_condition_test_suite,
    // connect_test_suite,
    coroutine_test_suite,
    // datagram_socket_service_test_suite,
    deadline_timer_test_suite,
    deadline_timer_service_test_suite,
    error_test_suite,
    // generic_datagram_protocol_test_suite,
    // generic_stream_protocol_test_suite,
    generic_basic_endpoint_test_suite,
    // generic_raw_protocol_test_suite,
    // generic_seq_packet_protocol_test_suite,
    high_resolution_timer_test_suite,
    // io_context_test_suite,
    ip_address_v4_test_suite,
    ip_basic_resolver_iterator_test_suite,
    ip_address_v6_iterator_test_suite,
    ip_host_name_test_suite,
    // ip_network_v4_test_suite,
    // ip_network_v6_test_suite,
    ip_address_v4_iterator_test_suite,
    ip_basic_resolver_entry_test_suite,
    // ip_resolver_service_test_suite,
    // ip_unicast_test_suite,
    ip_basic_endpoint_test_suite,
    // ip_multicast_test_suite,
    ip_address_v6_range_test_suite,
    // ip_basic_resolver_test_suite,
    // ip_icmp_test_suite,
    // ip_udp_test_suite,
    // ip_address_test_suite,
    // ip_tcp_test_suite,
    ip_basic_resolver_query_test_suite,
    // ip_address_v6_test_suite,
    // ip_v6_only_test_suite,
    ip_resolver_query_base_test_suite,
    ip_address_v4_range_test_suite,
    // is_read_buffered_test_suite,
    // is_write_buffered_test_suite,
    // local_connect_pair_test_suite,
    // local_datagram_protocol_test_suite,
    // local_stream_protocol_test_suite,
    local_basic_endpoint_test_suite,
    placeholders_test_suite,
    // posix_basic_descriptor_test_suite,
    // posix_basic_stream_descriptor_test_suite,
    // posix_stream_descriptor_test_suite,
    // posix_stream_descriptor_service_test_suite,
    posix_descriptor_base_test_suite,
    // raw_socket_service_test_suite,
    // read_at_test_suite,
    // read_test_suite,
    // read_until_test_suite,
    // seq_packet_socket_service_test_suite,
    // serial_port_base_test_suite,
    // serial_port_test_suite,
    // serial_port_service_test_suite,
    // signal_set_test_suite,
    // signal_set_service_test_suite,
    // socket_acceptor_service_test_suite,
    // socket_base_test_suite,
    // ssl_context_test_suite,
    // ssl_rfc2818_verification_test_suite,
    ssl_stream_base_test_suite,
    // ssl_stream_test_suite,
    // ssl_context_base_test_suite,
    // steady_timer_test_suite,
    // strand_test_suite,
    streambuf_test_suite,
    // stream_socket_service_test_suite,
    // system_timer_test_suite,
    thread_test_suite,
    time_traits_test_suite,
    waitable_timer_service_test_suite,
    wait_traits_test_suite,
    // windows_basic_object_handle_test_suite,
    // windows_object_handle_service_test_suite,
    // windows_basic_stream_handle_test_suite,
    // windows_random_access_handle_service_test_suite,
    // windows_random_access_handle_test_suite,
    // windows_basic_random_access_handle_test_suite,
    // windows_stream_handle_test_suite,
    // windows_basic_handle_test_suite,
    // windows_stream_handle_service_test_suite,
    // windows_overlapped_ptr_test_suite,
    // windows_object_handle_test_suite,
    // write_at_test_suite,
    // write_test_suite,
};

}  // namespace

int main() {
    long failing_tests = 0;
    std::vector<std::pair<const char*, long>> failing_suites;
    for (const auto& test_suite : test_suites) {
        auto outcome = test_suite();
        const char* name = outcome.first;
        long failures = outcome.second;
        if (failures > 0)
            failing_suites.push_back(std::make_pair(name, failures));
        failing_tests += failures;
    }

    ASIO_TEST_IOSTREAM << std::endl << std::endl;
    ASIO_TEST_IOSTREAM << "----------------------------------------" << std::endl;
    if (failing_tests == 0) {
        ASIO_TEST_IOSTREAM << "ASIO: All tests passed!" << std::endl;
        return 0;
    }

    ASIO_TEST_IOSTREAM << "ASIO: There were " << failing_tests;
    ASIO_TEST_IOSTREAM << " test failures total:" << std::endl;
    for (const auto& failing_suite : failing_suites) {
        const char* name = failing_suite.first;
        long failures = failing_suite.second;
        ASIO_TEST_IOSTREAM << "    " << name;
        ASIO_TEST_IOSTREAM << " (" << failures << " failures)" << std::endl;
    }
    return -1;
}
